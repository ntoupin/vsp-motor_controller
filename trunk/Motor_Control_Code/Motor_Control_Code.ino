/*
   @file Motor_Controller.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Motor control with encoder and current sensor.
*/

#include <NT_Pwm.h>
#include <NT_Motor.h>
#include <NT_Encoder.h>
#include <NT_Current_Sensor.h>
#include <NT_Pid.h>

Pwm_t Motor1_Pwm1;
Pwm_t Motor1_Pwm2;
Motor_t Motor1;
Encoder_t Encoder1;
Current_Sensor_t CurrentSensor1;
Pid_t Pid_Position;
Pid_t Pid_Speed;
Pid_t Pid_Current;

void setup()
{
  Motor1.Configure(0, 0, 0, 1000, 0, 256);
  Motor1.Init();

  Motor1_Pwm1.Configure(0, 0, Motor1.Minimum_Op, Motor1.Maximum_Op, 0, 255);
  Motor1_Pwm1.Attach(9);
  Motor1_Pwm1.Init();

  Motor1_Pwm2.Configure(0, 0, Motor1.Minimum_Op, Motor1.Maximum_Op, 0, 255);
  Motor1_Pwm2.Attach(10);
  Motor1_Pwm2.Init();

  Encoder1.Configure(0.1);
  Encoder1.Attach_Int(1, 2);
  Encoder1.Map_Int(Encoder1_Output_A, Encoder1_Output_B);
  Encoder1.Init();

  CurrentSensor1.Configure(0, 1023, 0, 5);
  CurrentSensor1.Attach(A0);
  CurrentSensor1.Init();

  Pid_Position.Configure(1, 0, 0);
  Pid_Position.Init();
  
  Pid_Speed.Configure(1, 0, 0);
  Pid_Speed.Init();
  
  Pid_Current.Configure(1, 0, 0);
  Pid_Current.Init();
}

void loop()
{
  float SP_Position = 10; // Setpoint in degree
  float SP_Speed = Pid_Position.Compute(SP_Position, Encoder1.Get_Position()); // Outputs speed
  float SP_Current = Pid_Speed.Compute(SP_Speed, Encoder1.Get_Speed()); // Outputs current
  float SP_Motor = Pid_Current.Compute(SP_Current, CurrentSensor1.Get()); // Outputs duty cycle
  Motor1.Set(SP_Motor);
  Motor1_Pwm1.Set(Motor1.Value1_Op);
  Motor1_Pwm2.Set(Motor1.Value2_Op);
}

void Encoder1_Output_A()
{
  Encoder1.Output_A.State = (bool)digitalRead(Encoder1.Output_A.Pin);
  Encoder1.Compute();
}

void Encoder1_Output_B()
{
  Encoder1.Output_B.State = (bool)digitalRead(Encoder1.Output_B.Pin);
}
